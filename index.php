<!DOCTYPE HTML>
<!--
	Theory by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<?php
require('includes/config.php'); ?>
<html>
	<head>
		<title><?php echo SITETITLE;?></title>
		<?php 
			$sql = mysql_query("SELECT * FROM siteinfo;");
			$row = mysql_fetch_object($sql);
			$meta = $row->meta;
			echo $meta;
		?>
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="<?php echo DIR;?>" class="logo"><?php echo SITETITLE;?></a>
					<nav id="nav">
						<a href="<?php echo DIR;?>">Home</a>
						<?php
						//get the rest of the pages
						$sql = mysql_query("SELECT * FROM pages WHERE isRoot='1' ORDER BY pageID");
						while ($row = mysql_fetch_object($sql))
						{
							echo "<a href=\"".DIR."?p=$row->pageID\">$row->pageTitle</a>";
						}
						?>
					</nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h1>Welcome to <?php echo SITETITLE;?></h1>
				<p><?php echo SITEDESC; ?></p>
			</section>

		<!-- Content Area -->
		<?php
		//if no page clicked on load home page default to it of 1
		if(!isset($_GET['p'])){
		$q = mysql_query("SELECT * FROM pages WHERE pageID='1'");
			} else { //load requested page based on the id
				$id = $_GET['p']; //get the requested id
				$id = mysql_real_escape_string($id); //make it safe for database use
				$q = mysql_query("SELECT * FROM pages WHERE pageID='$id'");
			}

		//get page data from database and create an object
		$r = mysql_fetch_object($q);
		?>
		<section id="three" class="wrapper">
				<div class="inner">
					<header class="align-center">
						<h2><?php echo $r->pageTitle ; ?></h2>
						<p><?php echo $r->pageDesc ; ?> </p>
					</header>
					<article>
					<?php echo $r->pageCont ; ?>
					</article>
					</div>
				</div>
			</section>

		

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<div class="flex">
						<div class="copyright">
							<?php
							//including footer file
							include('./admin/includes/footer.php');
							?>
							 Design: <a href="https://templated.co">TEMPLATED</a>. Images: <a href="https://unsplash.com">Unsplash</a>.
						</div>
						<ul class="icons">
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
							<li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
							<li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>
						</ul>
					</div>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>