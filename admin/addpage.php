<?php

require('../includes/config.php');

if(isset($_POST['submit'])){

	$title = $_POST['pageTitle'];
	$content = $_POST['pageCont'];
	$pageDesc = $_POST['pageDesc'];

	$title = mysql_real_escape_string($title);
	$content = mysql_real_escape_string($content);
	$pageDesc = mysql_real_escape_string($pageDesc);

	mysql_query("INSERT INTO pages (pageTitle,pageCont,pageDesc) VALUES ('$title','$content', '$pageDesc')")or die(mysql_error());
	$_SESSION['success'] = 'Page Added';
	header('Location: '.DIRADMIN);
	exit();

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo SITETITLE;?></title>
<!-- FOR CSS AND JS, INCLUDE link.php -->
<?php
include('./includes/link.php');
?>
</head>
<body>
<div id="wrapper">

<!-- nav bar - just include nav.php -->
<?php 
include('./includes/nav.php');
?>

<div id="content">

<h1>Add Page</h1>

<form action="" method="post">
<p>Title:<br /> <input name="pageTitle" type="text" value="" size="103" class="form-control" /></p>
<p>Page Description:<br /> <input name="pageDesc" type="text" value="" size="103" class="form-control" /></p>
<div class="alert alert-info">
  <strong>Info!</strong> Refer to the elements.html and generic.html source for more information about the theme elements and css classes(Only for free bundled theme "Theory").
</div>
<p>Content<br /><textarea name="pageCont" id="pageCont" cols="100" rows="20"><?php echo $row->pageCont;?></textarea>
<script type="text/javascript" >
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'pageCont' );
            </script>
</p>
<p><input type="submit" name="submit" value="Submit" class="btn btn-primary" /></p>
</form>

</div>

<!-- Footer - just include footer.php -->
<?php
include('./includes/footer.php');
?>
</div><!-- close wrapper -->
<script type="text/javascript" src="<?php echo DIRADMIN;?>scripts/main.js"></script>
</body>
</html>
