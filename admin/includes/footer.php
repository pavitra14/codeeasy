<?php
if (!defined('included')){
die('You cannot access this file directly!');
}
?>
<div id="footer" class="footer">
		<div class="copy">&copy; <?php echo SITETITLE.' '. date('Y');?> Powered by <a href="https://gitlab.com/pavitra14/codeeasy" target="_blank" >CodeEasy</a> </div>
</div>