<?php

if (!defined('included')){
die('You cannot access this file directly!');
}
?>
<!-- NAV -->
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo DIRADMIN; ?>">CodeEasy</a>
    </div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo DIRADMIN;?>">Admin <span class="sr-only">(current)</span></a></li>
        <li><a href="<?php echo DIR;?>" target="_blank">View Website</a></li>
				<li><a href="<?php echo DIRADMIN;?>editsite.php" onclick="exp();"> Edit Site Info</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo DIRADMIN;?>?logout"><span class="glyphicon">&#xe163;</span>Logout</a></li>
			</ul>
  </div>
</div>
</nav>
<!-- END NAV -->
  