<?php

 require('../includes/config.php');
if(logged_in()) {header('Location: '.DIRADMIN);}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo SITETITLE;?></title>
<link rel="stylesheet" href="<?php echo DIR;?>css/login.css" type="text/css" />
</head>
<body>
<div class="lwidth">
	<div class="page-wrap">
		<div class="content">

		<?php
		if($_POST['submit']) {
			login($_POST['username'], $_POST['password']);
		}
		?>

<div class="login">
	<p><?php echo messages();?></p>
  <div class="heading">
	  <img src='<?php echo DIR ;?>/images/codeeasy.png' height="138" width="369"><br />
    <h2>Sign in</h2>
    <form method="post" action="">

      <div class="input-group input-group-lg">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <input type="text" class="form-control" placeholder="username" name="username" required>
          </div>

        <div class="input-group input-group-lg">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input type="password" class="form-control" placeholder="Password" name="password" required>
        </div>

        <input type="submit" name="submit" class="button" value="Login" />
       </form>

		</div>
		<div class="clear"></div>
	</div>
<!-- Footer - just include footer.php -->
<?php
include('./includes/footer.php');
?>
</div>
</body>
</html>
