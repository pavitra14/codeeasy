<?php

require('../includes/config.php');

if(isset($_POST['submit'])){

	$title = $_POST['siteTitle'];
	$dir = $_POST['dir'];
	$diradmin = $_POST['diradmin'];
	$siteDesc = $_POST['siteDesc'];
	$meta = $_POST['meta'];

	$title = mysql_real_escape_string($title);
	$dir = mysql_real_escape_string($dir);
	$diradmin = mysql_real_escape_string($diradmin);
	$siteDesc = mysql_real_escape_string($siteDesc);
	//$meta = mysql_real_escape_string($meta);

	mysql_query("UPDATE siteinfo SET title='$title', dir='$dir', diradmin='$diradmin', sitedesc='$siteDesc', meta='$meta' WHERE id='1'");
	$_SESSION['success'] = 'Site info Updated';
	header('Location: '.DIRADMIN);
	exit();

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo SITETITLE;?></title>
<!-- FOR CSS AND JS, INCLUDE link.php -->
<?php
include('./includes/link.php');
?>
</head>
<body>
<div id="wrapper">

<!-- nav bar - just include nav.php -->
<?php 
include('./includes/nav.php');
?>

<div id="content">

<h1>Edit Site INFO</h1>

<?php
$id = $_GET['id'];
$id = mysql_real_escape_string($id);
$q = mysql_query("SELECT * FROM siteinfo");
$row = mysql_fetch_object($q);
?>


<form action="" method="post">
<input type="hidden" name="pageID" value="<?php echo $row->pageID;?>" />
<p>Site Title:<br /> <input name="siteTitle" type="text" value="<?php echo $row->title;?>" size="103" class="form-control"/>
</p>
<p>Site description:<br /><textarea name="siteDesc" id="siteDesc" cols="100" rows="20"><?php echo $row->sitedesc;?></textarea>
<script type="text/javascript" >
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'siteDesc' );
            </script>
</p>
<p>Site meta data code:<br /><textarea name="meta" cols="50" rows="5" class="form-control"><?php echo $row->meta;?></textarea>
<p>Site URL:<br /> <input name="dir" type="text" value="<?php echo $row->dir;?>" size="103" onclick="imp();" class="form-control"/>
<p>Admin URL:<br /> <input name="diradmin" type="text" value="<?php echo $row->diradmin;?>" size="103" onclick="imp();" class="form-control"/>
<p><input type="submit" name="submit" value="Submit" class="btn btn-primary" /></p>
</form>

</div>

<!-- Footer - just include footer.php -->
<?php
include('./includes/footer.php');
?>
</div><!-- close wrapper -->
<script type="text/javascript" src="<?php echo DIRADMIN;?>scripts/main.js"></script>
</body>
</html>
