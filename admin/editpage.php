<?php

require('../includes/config.php');

if(!isset($_GET['id']) || $_GET['id'] == ''){ //if no id is passed to this page take user back to previous page
	header('Location: '.DIRADMIN);
}

if(isset($_POST['submit'])){

	$title = $_POST['pageTitle'];
	$content = $_POST['pageCont'];
	$pageID = $_POST['pageID'];
	$pageDesc = $_POST['pageDesc'];

	$title = mysql_real_escape_string($title);
	$content = mysql_real_escape_string($content);
	$pageID = mysql_real_escape_string($pageID);
	$pageDesc = mysql_real_escape_string($pageDesc);

	mysql_query("UPDATE pages SET pageTitle='$title', pageCont='$content', pageDesc='$pageDesc' WHERE pageID='$pageID'");
	$_SESSION['success'] = 'Page Updated';
	header('Location: '.DIRADMIN);
	exit();

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo SITETITLE;?></title>
<!-- FOR CSS AND JS, INCLUDE link.php -->
<?php
include('./includes/link.php');
?>
</head>
<body>
<div id="wrapper">

<!-- nav bar - just include nav.php -->
<?php 
include('./includes/nav.php');
?>

<div id="content">

<h1>Edit Page</h1>

<?php
$id = $_GET['id'];
$id = mysql_real_escape_string($id);
$q = mysql_query("SELECT * FROM pages WHERE pageID='$id'");
$row = mysql_fetch_object($q);
?>


<form action="" method="post">
<input type="hidden" name="pageID" value="<?php echo $row->pageID;?>" />
<p>Title:<br /> <input name="pageTitle" type="text" value="<?php echo $row->pageTitle;?>" size="103" class="form-control"/>
</p>
<p>Page Description:<br /> <input name="pageDesc" type="text" value="<?php echo $row->pageDesc;?>" size="103" class="form-control"/>
</p>
<div class="alert alert-info">
  <strong>Info!</strong> Refer to the elements.html and generic.html source for more information about the theme elements and css classes(Only for free bundled theme "Theory").
</div>
<p>Content<br /><textarea name="pageCont" id="pageCont" cols="100" rows="20"><?php echo $row->pageCont;?></textarea>
<script type="text/javascript" >
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'pageCont' );
            </script>
</p>
<p><input type="submit" name="submit" value="Submit" class="btn btn-primary" /></p>
</form>

</div>

<!-- Footer - just include footer.php -->
<?php
include('./includes/footer.php');
?>
</div><!-- close wrapper -->
<script type="text/javascript" src="<?php echo DIRADMIN;?>scripts/main.js"></script>
</body>
</html>
